#line 1 "./HiddenValleyModel.cc"

// -*- C++ -*-
//
// This is the implementation of the non-inlined, non-templated member
// functions of the HiddenValleyModel class.
//

#include "HiddenValleyModel.h"
#include "ThePEG/Utilities/DescribeClass.h"
#include "ThePEG/Interface/ClassDocumentation.h"
#include "ThePEG/Interface/Parameter.h"
#include "ThePEG/Interface/Reference.h"
#include "ThePEG/Persistency/PersistentOStream.h"
#include "ThePEG/Persistency/PersistentIStream.h"

using namespace Herwig;
using namespace ThePEG;
using namespace ThePEG::Helicity;

void HiddenValleyModel::doinit()  {
  addVertex(_theDGGCHVertex);
  
  BSMModel::doinit();
}

HiddenValleyModel::HiddenValleyModel() :  _CouplHV(1.0)
{}


IBPtr HiddenValleyModel::clone() const {
  return new_ptr(*this);
}
IBPtr HiddenValleyModel::fullclone() const {
  return new_ptr(*this);
}


// If needed, insert default implementations of virtual function defined
// in the InterfacedBase class here (using ThePEG-interfaced-impl in Emacs).


void HiddenValleyModel::persistentOutput(PersistentOStream & os) const {
  os << _theDGGCHVertex
    << _CouplHV;
    
  
}

void HiddenValleyModel::persistentInput(PersistentIStream & is, int) {
  is >> _theDGGCHVertex
     >> _CouplHV;
    
  
}

// The following static variable is needed for the type
// description system in ThePEG.
DescribeClass<HiddenValleyModel,BSMModel>
describeHerwigHiddenValleyModel("Herwig::HiddenValleyModel", "HwHiddenValleyModel.so");

void HiddenValleyModel::Init() {
  

  static Reference<HiddenValleyModel,ThePEG::Helicity::AbstractVVSVertex> interfaceVertexDGGCH
  ("Vertex/DGGCH",
   "Reference to the dark gluon-gluon-chi(scalar) vertex",
   &HiddenValleyModel::_theDGGCHVertex, false, false, true, false, false);

  static Parameter<HiddenValleyModel, double> interfaceHVCoupling
    ("HVCoupling",
     "The overall HiddenValley Coupling",
     &HiddenValleyModel::_CouplHV, 1.0, 0., 10.0,
     false, false, Interface::limited);

  static ClassDocumentation<HiddenValleyModel> documentation
    ("There is no documentation for the HiddenValleyModel class");

}

#line 1 "./HiddenValleyModelDGGCHVertex.cc"
// -*- C++ -*-
//
// HiddenValleyModelDGGCHVertex.cc is a part of Herwig - A multi-purpose Monte Carlo event generator
// Copyright (C) 2002-2019 The Herwig Collaboration
//
// Herwig is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic guidelines, see GUIDELINES for details.
//
//
// This is the implementation of the non-inlined, non-templated member
// functions of the HiddenValleyModelDGGCHVertex class.
//

#include "HiddenValleyModelDGGCHVertex.h"
#include "ThePEG/Utilities/DescribeClass.h"
#include "ThePEG/Interface/ClassDocumentation.h"


using namespace Herwig;
using namespace ThePEG;

HiddenValleyModelDGGCHVertex::HiddenValleyModelDGGCHVertex() : q2last_(ZERO),
								 couplast_(0.) {
  orderInGs(0);
  orderInGem(0);
  colourStructure(ColourStructure::SU3TFUND);
}

void HiddenValleyModelDGGCHVertex::doinit() {
  addToList(90,21,70);
  VVSVertex::doinit();
}

// The following static variable is needed for the type
// description system in ThePEG.
DescribeNoPIOClass<HiddenValleyModelDGGCHVertex,VVSVertex>
describeHerwigHiddenValleyModelDGGCHVertex("Herwig::HiddenValleyModelDGGCHVertex", "HwHiddenValleyModel.so");

void HiddenValleyModelDGGCHVertex::Init() {
  static ClassDocumentation<HiddenValleyModelDGGCHVertex> documentation
    ("The HiddenValleyModelDGGCHVertex class is the implementation of"
     " the HiddenValleyModel Dark Gluon - Gluon - Chi (neutral) vertex");
  
}

void HiddenValleyModelDGGCHVertex::setCoupling(Energy2 q2,tcPDPtr ,tcPDPtr ,tcPDPtr) {
  if(q2 != q2last_ || couplast_ == 0.) {
    couplast_ = 1.0;  
    q2last_ = q2;
  }
  norm(couplast_);
}
