// -*- C++ -*-
#ifndef HERWIG_HiddenValleyModel_H
#define HERWIG_HiddenValleyModel_H
//
// This is the declaration of the HiddenValleyModel class.
//

#include "Herwig/Models/General/BSMModel.h"
#include "ThePEG/Helicity/Vertex/AbstractVVSVertex.h"
#include "HiddenValleyModel.fh"

namespace Herwig {

using namespace ThePEG;
using namespace ThePEG::Helicity;

/**
 * Here is the documentation of the HiddenValleyModel class.
 *
 * @see \ref HiddenValleyModelInterfaces "The interfaces"
 * defined for HiddenValleyModel.
 */
class HiddenValleyModel: public BSMModel {

public:

  /**
   * The default constructor.
   */
  HiddenValleyModel();
 
  /** @name Vertices */
  //@{
  /**
   * Pointer to the object handling S0S0barg vertex.
   */
  tAbstractVVSVertexPtr   vertexDGGCH() const {return _theDGGCHVertex;}
  
 
public:

  /** @name Functions used by the persistent I/O system. */
  //@{
  /**
   * Function used to write out object persistently.
   * @param os the persistent output stream written to.
   */
  void persistentOutput(PersistentOStream & os) const;

  /**
   * Function used to read in object persistently.
   * @param is the persistent input stream read from.
   * @param version the version number of the object when written.
   */
  void persistentInput(PersistentIStream & is, int version);
  //@}

  /**
   * The standard Init function used to initialize the interfaces.
   * Called exactly once for each class by the class description system
   * before the main function starts or
   * when this class is dynamically loaded.
   */
  static void Init();

 /**
   * Return the overall fermion coupling
   */
  double OverallCoupling() const {return _CouplHV;}

protected:

  /** @name Standard Interfaced functions. */
  //@{
  /**
   * Initialize this object after the setup phase before saving an
   * EventGenerator to disk.
   * @throws InitException if object could not be initialized properly.
   */
  virtual void doinit();
  //@}

protected:

  /** @name Clone Methods. */
  //@{
  /**
   * Make a simple clone of this object.
   * @return a pointer to the new object.
   */
  virtual IBPtr clone() const;

  /** Make a clone of this object, possibly modifying the cloned object
   * to make it sane.
   * @return a pointer to the new object.
   */
  virtual IBPtr fullclone() const;

  
    
  
  //@}


// If needed, insert declarations of virtual function defined in the
// InterfacedBase class here (using ThePEG-interfaced-decl in Emacs).


private:

  /**
   * The assignment operator is private and must never be called.
   * In fact, it should not even be implemented.
   */
  HiddenValleyModel & operator=(const HiddenValleyModel &) = delete;

  
  /**
   * Pointer to the object handling the G to SLQ SLQ vertex.
   */
  AbstractVVSVertexPtr  _theDGGCHVertex;


  /**
   *  Overall coupling
   */
  double _CouplHV;
};

}

#endif /* HERWIG_HiddenValleyModel_H */
