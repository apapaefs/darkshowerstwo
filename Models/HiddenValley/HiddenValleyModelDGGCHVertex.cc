// -*- C++ -*-
//
// HiddenValleyModelDGGCHVertex.cc is a part of Herwig - A multi-purpose Monte Carlo event generator
// Copyright (C) 2002-2019 The Herwig Collaboration
//
// Herwig is licenced under version 3 of the GPL, see COPYING for details.
// Please respect the MCnet academic guidelines, see GUIDELINES for details.
//
//
// This is the implementation of the non-inlined, non-templated member
// functions of the HiddenValleyModelDGGCHVertex class.
//

#include "HiddenValleyModelDGGCHVertex.h"
#include "ThePEG/Utilities/DescribeClass.h"
#include "ThePEG/Interface/ClassDocumentation.h"


using namespace Herwig;
using namespace ThePEG;

HiddenValleyModelDGGCHVertex::HiddenValleyModelDGGCHVertex() : q2last_(ZERO),
								 couplast_(0.) {
  orderInGs(0);
  orderInGem(0);
  colourStructure(ColourStructure::SU3TFUND);
}

void HiddenValleyModelDGGCHVertex::doinit() {
  addToList(90,21,70);
  VVSVertex::doinit();
}

// The following static variable is needed for the type
// description system in ThePEG.
DescribeNoPIOClass<HiddenValleyModelDGGCHVertex,VVSVertex>
describeHerwigHiddenValleyModelDGGCHVertex("Herwig::HiddenValleyModelDGGCHVertex", "HwHiddenValleyModel.so");

void HiddenValleyModelDGGCHVertex::Init() {
  static ClassDocumentation<HiddenValleyModelDGGCHVertex> documentation
    ("The HiddenValleyModelDGGCHVertex class is the implementation of"
     " the HiddenValleyModel Dark Gluon - Gluon - Chi (neutral) vertex");
  
}

void HiddenValleyModelDGGCHVertex::setCoupling(Energy2 q2,tcPDPtr ,tcPDPtr ,tcPDPtr) {
  if(q2 != q2last_ || couplast_ == 0.) {
    couplast_ = 1.0;  
    q2last_ = q2;
  }
  norm(couplast_);
}
